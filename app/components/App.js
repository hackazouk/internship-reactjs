/* jshint esversion: 6 */


var React = require('react'); 
var ReactRouter = require('react-router-dom'); 

var Router = ReactRouter.BrowserRouter;
var Route = ReactRouter.Route;
var Switch = ReactRouter.Switch;

// components
import Home from './Home';
import Navigation from './Navigation';
import Contact from "./Contact";

class App extends React.Component {
  render() { 
    return (
      <Router>
        <div className="container">
          <Navigation />
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/contact" component={Contact} />
            
            <Route exact path="/about" render={function () {
              return <p>About us <a href="/about/address"></a></p>
            }} />

            <Route exact path="/about/address" render={function () {
              return <p>About page with address</p>
            }} />
             
            <Route render={function(){
              return <p className="text-center site404Error"> 404: Not found!</p>
            }} />
          </Switch>
        </div>
      </Router>
    )
  }
} 

module.exports = App;