import React, { Component } from 'react'
import PropTypes from 'prop-types';
import ActualMap from './ActualMap';

class Map extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url:"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2343.4848627038878!2d121.05341873776734!3d14.647638786961977!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397b70b628cf95f%3A0x9bf6d402128b4dc5!2s128-A+Malumanay%2C+Diliman%2C+Lungsod+Quezon%2C+1101+Kalakhang+Maynila!5e0!3m2!1sen!2sph!4v1527753245005",
      width:1000,
      height:450
    }
  }

  componentDidMount() {
    console.log("did component did mount?");
    setTimeout(() => {
      console.log("Yes, Component did mount!");
    }, 3000);
  }
  
  componentWillUnmount() {
    console.log('will unmount');
  }
  
  // component life cycle

  render() {
    return (
      <div> 

        <ActualMap 
          url={this.state.url}
          width={this.state.width}
          height={this.state.height}
        >
          {"this is the map of our office"}
          <h3>Did you see it?</h3>
          <strong>Nice!</strong>
        </ActualMap>
        
      </div>
    )
  }
}


Map.propTypes = {
  address: PropTypes.string.isRequired
}
Map.defaultProps = {
  address : "No address found!"
}

module.exports = Map;