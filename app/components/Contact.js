import React, { Component } from 'react'
import Map from './Map';
import ContactInfo from './ContactInfo';
class Contact extends Component {

  constructor (props)  {
    super(props);
    this.state = {
      address: "128 Malumanay st,. UP Village, Quezon City",
      owner : "Paolo Agloro",
      contact: "123124124124",
      description: "Description"
    };

    this.updateInfo = this.updateInfo.bind(this);
  }

  updateInfo (description) {
    this.setState({
      description: description
    });
  }

  render() {
    return (
      <div>
        <h1>CONTACT US</h1>
        <ContactInfo 
          address={this.state.address}
          owner={this.state.owner}
          contact={this.state.contact}
          description={this.state.description}
          updateInfo={this.updateInfo}
        />
        <Map address={this.state.address} />
      </div>
    )
  }
}

module.exports = Contact;

// https://reactjs.org/docs/react-component.html