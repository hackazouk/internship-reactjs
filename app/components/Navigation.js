import React, { Component } from 'react'
var NavLink = require('react-router-dom').NavLink;

class Navigation extends Component {

  
  render() {
    return (
      <div className="navigation">
        <ul>
          <li>
            <NavLink exact activeClassName='active' to='/'>
              Home
            </NavLink>
          </li>
          <li>
            <NavLink exact activeClassName='active' to='/about'>
              About
            </NavLink>
          </li>
          <li>
            <NavLink exact activeClassName='active' to='/contact'>
              contact
            </NavLink>  
          </li>
        </ul>
      </div>
    )
  }
}

module.exports = Navigation;
