import React, { Component } from 'react'

class ContactInfo extends Component {

  constructor(props) {
    super(props);
    this.changeDescription = this.changeDescription.bind(this);
  }

  changeDescription(e) {
    var value = e.target.value;
    this.props.updateInfo(value);
  }

  componentDidUpdate(prevProps, prevState) {
    console.log('component did update from:',prevProps.description, "to",this.props.description);
  }

  render() {
    return (
      <div>
        <ul>
          <li>{this.props.owner}</li>
          <li>{this.props.contact}</li>
          <li>{this.props.address}</li> 
        </ul>
        <p>{this.props.description}</p>
        <textarea onChange={this.changeDescription}/>  
      </div>
    )
  }
} 

module.exports = ContactInfo;
