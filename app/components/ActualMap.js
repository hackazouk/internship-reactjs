import React, { Component } from 'react'
import PropTypes from 'prop-types'

function ActualMap(props) {
  return (
    <div>
      <iframe 
        src={props.url} 
        width={props.width} 
        height={props.height} 
        frameBorder="0"  
        allowFullScreen>
      </iframe>
      <div>
        {props.children}
      </div> 
    </div>
  );
}

module.exports = ActualMap;


